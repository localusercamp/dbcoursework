require('./bootstrap');

import Vue from 'vue';
import { router } from './router/routes';
import { store } from './vuex/store';
import { sync } from 'vuex-router-sync';
import App from './App';
import FishUI from 'fish-ui';

Vue.use(FishUI);
Vue.use(DatePicker);

router.beforeEach((to, from, next)=>{
  // исполняется перед каждым переходом роутера
  next();
});

router.afterEach((to, from)=>{
  // исполняется после каждого перехода роутера
});

sync(store, router);

const VueApp = new Vue({
  router,
  store,
  el: '#app',
  template: '<App/>',
  components: { App },
  created() {
    // всякая всячина которая должна выполнятся "первой".
  },
});

export default VueApp;
