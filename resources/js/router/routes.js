import Vue from 'vue';
import VueRouter from 'vue-router'
import {store} from '~/vuex/store'
import axios from 'axios'

Vue.use(VueRouter);

// Приписсывание заголовка всем запросам (заголовок авторизации)
// -------------------------------------------------------------
// axios.interceptors.request.use(
//   config => {
//     const access_token = store.getters.access_token;
//     if (store.getters.isAuthenticated) config.headers.Authorization = `Bearer ${access_token}`;
//     return config;
//   },
//   error => Promise.reject(error)
// );

// Авторизован или нет
// -------------------
// const ifNotAuthenticated = (to, from, next) => {
//   if (!store.getters.isAuthenticated) {
//     next();
//     return
//   }
//   next('/dashboard')
// }
// const ifAuthenticated = (to, from, next) => {
//   if (store.getters.isAuthenticated) {
//     next();
//     return
//   }
//   next('/')
// }

// Пример регистрации роута
// ------------------------
// const Home = () => import(/* webpackChunkName: "Home" */ '../views/Home.vue');

export const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/client/market',
    children: [
      // Здесь находятся все роуты которые будут доступны по схеме: .../client/market/[children path]
    ],
  }, {
    path: '/signin',
    name: 'signin',
    component: Signin,
    beforeEnter: ifNotAuthenticated,
  }, {
    path: '/signup',
    name: 'signup',
    component: Signup,
    beforeEnter: ifNotAuthenticated,
  }, {
    path: '*',
    redirect: '/',
  },
];

export const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  routes
});
