import Vue from 'vue';
import Vuex from 'vuex';
// import createPersistedState from "vuex-persistedstate"; // импорт штуки кооторая позволяет при перезагрузке хранить данные
import { router } from '../router/routes';

// import broadcast from './modules/broadcast'; // импорт модуля

Vue.use(Vuex);

export const store = new Vuex.Store({
  // plugins: [createPersistedState()], // если есть [import createPersistedState]

  modules: {
  },

  state: {
  },

  actions: {
    // Пример экшона с возвратом промиса
    // ----------------------------------
    // signin({commit, dispatch}, user) {
    //   return new Promise((resolve, reject) => {
    //     axios.post('/api/auth/signin', {
    //       email: user.email,
    //       password: user.password
    //     })
    //     .then((r) => {
    //       let access_token = r.data.access_token;
    //       commit('set_access_token', access_token);
    //       resolve();
    //     })
    //     .catch((err) => {
    //       reject(err);
    //     });
    //   });
    // },
  },

  mutations: {
    // Пример мутации на запись токена в стор
    // --------------------------------------
    // set_access_token (state, access_token) {
    //   state.access_token = access_token;
    // },
  },

  getters: {
    // Пример геттера на токен
    // -----------------------
    // access_token: state => {
    //   return state.access_token;
    // },
  }

});
